var express = require('express');
var router = express.Router();
var multiparty = require('multiparty');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'GPS Data Listener' });
});

router.all('/endpoint', (req, res ,next) => {
  var data = { 
    query : req.query ,
    body : req.body,
    prams : req.params
  };

  var form = new multiparty.Form();
  form.parse(req, function(err, fields, files) {
      // fields fields fields
      data.formData = fields;
      console.log(data);
      res.io.emit("data", data);
      res.send("ok");
  });


});

module.exports = router;
